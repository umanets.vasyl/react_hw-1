import "./App.scss";
import Button from "./components/Button";
import Modal from "./components/Modal";
import React, { Component } from "react";

class App extends Component {
  state = {
    isFirstModalOpen: false,
    isSecondModalOpen: false,
  };

  openModal(e) {
    console.log(this.state, e);
    this.setState({
      ...this.state,
      [e]: !this.state[e],
    });
  }

  

  render() {
    return (
      <div>
        <div className="container__main-centered">
          <Button
            text="delete"
            backgroundColor="red"
            onClick={() => this.openModal("isFirstModalOpen")}
          />
          <Button
            text="create"
            backgroundColor="teal"
            onClick={() => this.openModal("isSecondModalOpen")}
          />
        </div>
        {this.state.isFirstModalOpen && (
          <Modal
            onClick={() => this.openModal("isFirstModalOpen")}
            header={"Do you want to delete this file?"}
            text={
              "Once your delete this file, it won't be possible to undo this action.Are you sure you want to delete it?"
            }
            action={
              <div className="action-content">
                <Button text="OK" backgroundColor="FireBrick" />
                <Button
                  text="Cancel"
                  backgroundColor="FireBrick"
                  onClick={() => this.openModal("isFirstModalOpen")}
                />
              </div>
            }
          />
        )}

        {this.state.isSecondModalOpen && (
          <Modal
            onClick={() => this.openModal("isSecondModalOpen")}
            header={"Do you want create new file?"}
            text={"Click the button below to create a file"}
            action={
              <div className="action-content">
                <Button text="Create" backgroundColor="Teal" />
                <Button
                  text="Cancel"
                  backgroundColor="Teal"
                  onClick={() => this.openModal("isSecondModalOpen")}
                />
              </div>
            }
          />
        )}
      </div>
    );
  }
}

export default App;
