import React from "react";
import "./style.scss";

class Modal extends React.Component {
  render() {
    return (
      <div onClick={this.props.onClick}>
        <div className="modal-container">
          <section className="modal">
            <header className="modal-header">
              <h2 className="modal-title">{this.props.header}</h2>
              <a
                href="#"
                className="modal-close"
                onClick={this.props.onClick}
              ></a>
            </header>
            <div className="modal-content">
              <p>{this.props.text}</p>
            </div>
            <div>{this.props.action}</div>
          </section>
        </div>
        ;
      </div>
    );
  };
}

export default Modal;
